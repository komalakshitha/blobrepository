const mongoose=require("mongoose");
const UserSchema=mongoose.Schema({
    author_id:{
        type:String,
    },
    author_name:{
        type:String,
    },
    description:{
        type:String,
    },
    title:{
        type:String,
    },
    date:{
        type:Date,
    }
})
const User=mongoose.model("Blog",UserSchema);
module.exports=User;