const express = require("express");
const router = express.Router();
const User = require("../models/Users");

router.post("/postUser", async (req, res) => {
  try {
    const { author_id, author_name, description, title, date } = req.body;
    const user = new User({ author_id, author_name, description, title, date });
    await user.save();

    res.status(200).json({ "id": user.id });
  } catch (err) {
    res.status(400).json({ "err": err.message });
  }
});
router.get('/getAllUser',async (req,res)=>{
    try{

        const users = await User.find();
        res.status(200).json(users);
    }
    catch(err)
    {
        res.status(400).json({"err":err.message});
    }
});
router.delete('/deleteUser/:id',async(req,res)=>{
    try{
        await User.findByIdAndDelete(req.params.id);``

        res.status(200).json({"message":"User Deleted", "id": req.params.id});
    }
    catch(err)
    {
        res.status(400).json({"err":err.message});
    }

});
router.put('/updateUser/:id', async (req,res)=>{
  try{

      const user = await User.findById(req.params.id);
      user.name = req.body.name;

      await user.save();
      res.status(200).json(user);
  } catch(err) {
      res.status(400).json({"err":err.message});
  }
})

module.exports = router;
