const express = require("express");
const app = express();
app.use(express.json());
const connectDB=require("./config/db");
connectDB();
app.get("/", (req, res) => res.send("APIs running"));
app.use('/api/users',require("./routes/users"));
app.listen(5000, () => console.log("server is running at port 5000"));

